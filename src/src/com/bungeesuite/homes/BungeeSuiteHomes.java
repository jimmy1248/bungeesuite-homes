package com.bungeesuite.homes;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import com.bungeesuite.homes.commands.DelHomeCommand;
import com.bungeesuite.homes.commands.HomeCommand;
import com.bungeesuite.homes.commands.HomesCommand;
import com.bungeesuite.homes.commands.ImportHomesCommand;
import com.bungeesuite.homes.commands.ReloadHomesCommand;
import com.bungeesuite.homes.commands.SetHomeCommand;
import com.bungeesuite.homes.listners.HomesListener;
import com.bungeesuite.homes.listners.HomesMessageListener;
import com.bungeesuite.teleports.BungeeSuiteTeleports;

public class BungeeSuiteHomes extends JavaPlugin {

	public static BungeeSuiteHomes instance;

	public static String OUTGOING_PLUGIN_CHANNEL = "BSHomes";
	static String INCOMING_PLUGIN_CHANNEL = "BungeeSuiteHomes";
	public static boolean usingTeleports = false;

	@Override
	public void onEnable() {
		instance = this;
		registerListeners();
		registerChannels();
		registerCommands();
		BungeeSuiteTeleports bt = (BungeeSuiteTeleports) Bukkit.getPluginManager().getPlugin("Teleports");
		if(bt!=null){
			if(bt.getDescription().getAuthors().contains("Bloodsplat")){
				usingTeleports = true;
			}
		}
	}
	
	private void registerCommands() {
		getCommand("sethome").setExecutor(new SetHomeCommand());
		getCommand("home").setExecutor(new HomeCommand());
		getCommand("delhome").setExecutor(new DelHomeCommand());
		getCommand("homes").setExecutor(new HomesCommand());
		getCommand("importhomes").setExecutor(new ImportHomesCommand());
	}

	private void registerChannels() {
		Bukkit.getMessenger().registerIncomingPluginChannel(this,
				INCOMING_PLUGIN_CHANNEL, new HomesMessageListener());
		Bukkit.getMessenger().registerOutgoingPluginChannel(this,
				OUTGOING_PLUGIN_CHANNEL);
	}

	private void registerListeners() {
		getServer().getPluginManager().registerEvents(
				new HomesListener(), this);
	}


}
