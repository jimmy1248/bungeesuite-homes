package com.bungeesuite.homes.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import com.bungeesuite.homes.managers.HomesManager;

public class ReloadHomesCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command,
			String label, String[] args) {
		HomesManager.reloadHomes(sender);
		return true;
	}

}
